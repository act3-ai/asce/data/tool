## ace-dt info

View detailed documentation for the tool

### Synopsis

The info command provides detailed documentation in your terminal.

### Options

```
  -h, --help   help for info
```

### Options inherited from parent commands

```
      --config stringArray         configuration file location (setable with env "ACE_DT_CONFIG").
                                   The first configuration file present is used.  Others are ignored.
                                    (default [ace-dt-config.yaml,HOMEDIR/.config/ace/dt/config.yaml,/etc/ace/dt/config.yaml])
  -v, --verbosity strings[=warn]   Logging verbosity level (also setable with environment variable ACE_DT_VERBOSITY)
                                   Aliases: error=0, warn=4, info=8, debug=12 (default [error])
```

### SEE ALSO

* [ace-dt](ace-dt.md)	 - data management tool for bottles and artifacts
* [ace-dt info anatomy](ace-dt_info_anatomy.md)	 - Anatomy of a Bottle
* [ace-dt info bottle-creator-guide](ace-dt_info_bottle-creator-guide.md)	 - Bottle Creator Guide
* [ace-dt info config-v1alpha1](ace-dt_info_config-v1alpha1.md)	 - Configuration API Documentation
* [ace-dt info faq](ace-dt_info_faq.md)	 - FAQ
* [ace-dt info labels](ace-dt_info_labels.md)	 - Bottle Labels and Selectors
* [ace-dt info mirror-tutorial](ace-dt_info_mirror-tutorial.md)	 - Mirror Tutorial
* [ace-dt info mirror-user-guide](ace-dt_info_mirror-user-guide.md)	 - Mirror User Guide
* [ace-dt info quick-start-guide](ace-dt_info_quick-start-guide.md)	 - Quick Start Guide
* [ace-dt info registry-config](ace-dt_info_registry-config.md)	 - Registry Configuration Options
* [ace-dt info user-guide](ace-dt_info_user-guide.md)	 - User Guide

