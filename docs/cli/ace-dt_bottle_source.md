## ace-dt bottle source

A command group for bottle source operations

### Options

```
  -h, --help   help for source
```

### Options inherited from parent commands

```
  -d, --bottle-dir string          Specify bottle directory (default "/builds/ace/data/tool")
      --config stringArray         configuration file location (setable with env "ACE_DT_CONFIG").
                                   The first configuration file present is used.  Others are ignored.
                                    (default [ace-dt-config.yaml,HOMEDIR/.config/ace/dt/config.yaml,/etc/ace/dt/config.yaml])
  -v, --verbosity strings[=warn]   Logging verbosity level (also setable with environment variable ACE_DT_VERBOSITY)
                                   Aliases: error=0, warn=4, info=8, debug=12 (default [error])
```

### SEE ALSO

* [ace-dt bottle](ace-dt_bottle.md)	 - A command group for common data bottle operations
* [ace-dt bottle source add](ace-dt_bottle_source_add.md)	 - add source information to bottle
* [ace-dt bottle source list](ace-dt_bottle_source_list.md)	 - list source information from a bottle
* [ace-dt bottle source remove](ace-dt_bottle_source_remove.md)	 - remove source information from a bottle

