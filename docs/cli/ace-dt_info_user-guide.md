## ace-dt info user-guide

User Guide

### Synopsis

View the "User Guide" document in your terminal.

```
ace-dt info user-guide [flags]
```

### Options

```
  -h, --help                 help for user-guide
  -w, --write string[="."]   write the document to a Markdown file (optionally specify a target directory)
```

### Options inherited from parent commands

```
      --config stringArray         configuration file location (setable with env "ACE_DT_CONFIG").
                                   The first configuration file present is used.  Others are ignored.
                                    (default [ace-dt-config.yaml,HOMEDIR/.config/ace/dt/config.yaml,/etc/ace/dt/config.yaml])
  -v, --verbosity strings[=warn]   Logging verbosity level (also setable with environment variable ACE_DT_VERBOSITY)
                                   Aliases: error=0, warn=4, info=8, debug=12 (default [error])
```

### SEE ALSO

* [ace-dt info](ace-dt_info.md)	 - View detailed documentation for the tool

