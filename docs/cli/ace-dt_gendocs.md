## ace-dt gendocs

Generate documentation for the tool in various formats

### Options

```
  -h, --help   help for gendocs
```

### Options inherited from parent commands

```
      --config stringArray         configuration file location (setable with env "ACE_DT_CONFIG").
                                   The first configuration file present is used.  Others are ignored.
                                    (default [ace-dt-config.yaml,HOMEDIR/.config/ace/dt/config.yaml,/etc/ace/dt/config.yaml])
  -v, --verbosity strings[=warn]   Logging verbosity level (also setable with environment variable ACE_DT_VERBOSITY)
                                   Aliases: error=0, warn=4, info=8, debug=12 (default [error])
```

### SEE ALSO

* [ace-dt](ace-dt.md)	 - data management tool for bottles and artifacts
* [ace-dt gendocs html](ace-dt_gendocs_html.md)	 - Generate documentation in HTML format
* [ace-dt gendocs man](ace-dt_gendocs_man.md)	 - Generate documentation in manpage format
* [ace-dt gendocs md](ace-dt_gendocs_md.md)	 - Generate documentation in Markdown format

